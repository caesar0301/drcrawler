package com.cuandas.zbc.crawlers

import java.sql.{Connection, DriverManager, Statement}
import java.text.{ParseException, SimpleDateFormat}

import com.cuandas.zbc.Utils
import com.cuandas.zbc.Utils._
import com.microsoft.sqlserver.jdbc.SQLServerException
import org.openqa.selenium.{By, WebElement}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex
import scala.collection.JavaConversions._

private case class DRProjectInfo
(
  var projectNumber: String = "",    // 项目编码
  var projectName: String = "",      // 项目名称
  var docNumber: String = "",        // 审批文号
  var docName: String = "",          // 审批文件
  var issuedDep: String = "",        // 批复机关
  var issuedTime: String = "",       // 批复时间
  var acceptDep: String = "",        // 受文单位
  var legalEntity: String = "",      // 项目法人
  var constLocation: String = "",    // 建设地点
  var constTarget: String = "",      // 建设内容
  var constScale: String = ""        // 建设规模
) {
  def format(): String = {
    "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')".format(
      projectNumber,
      projectName,
      docNumber,
      docName,
      issuedDep,
      issuedTime,
      acceptDep,
      legalEntity,
      constLocation,
      constTarget,
      constScale
    )
  }
}

/**
  * 上海市发展与改革委员会 - 项目审批
  */
object DRCrawler extends WebCrawler {
  private val HOME_SITE = "http://www.shdrc.gov.cn/info/iList.jsp?node_id=GKxxgk&cat_id=10066&q=5"
  private val DATABASE_NAME = "crawler"
  val TABLE_NAME = "crawler.devref"
  val TABLE_JSDW = "crawler.devref_jsdw"

  private var validConstructors = List.empty[String]
  @volatile private var morePageExists = true
  @volatile private var succeededKeys = List.empty[String]

  /**
    * Main portal
    */
  def main(args: Array[String]): Unit = {

    validateDatabaseContext()

    // Read jsdw to filter records
    val con = DriverManager.getConnection(Utils.JDBC_LINK)
    val stmt = con.createStatement()

    val rs = stmt.executeQuery(s"select * from ${TABLE_JSDW}")
    val names = new mutable.ListBuffer[String]
    while(rs.next()) {
      names.append(rs.getString("name"))
    }
    validConstructors = names.toList
    rs.close()

    stmt.close()
    con.close()

    // Start parsing
    Utils.clearRunningContext()
    start(keys=List(""))
  }

  /**
    * Internal dirty work
    */
  override def searchInternal(key: String): Unit = {
    logger.info(s"Searching ${key}")
    val results = new ListBuffer[DRProjectInfo]

    driver.navigate().to(HOME_SITE)
    val mainWindowId = driver.getWindowHandle

    // temporal range
    val sdf = new SimpleDateFormat("yyyy-MM-dd")
    val (startDate, endDate) = historyInterval(HISTORY_DAYS_DR)
    val startDateTime = sdf.parse(startDate)

    // extract total page counter
    val (currentPage, totalPages) = totalPage()

    var i = 0
    val projects = new mutable.HashMap[String, DRProjectInfo]()

    while(i < totalPages && morePageExists) {

      val rows: List[WebElement] = driver.findElementsByXPath("//div[@class='xwzx_list']/ul/li").toList
      logger.info(s"Total ${rows.length} submissions found for current page ${i+1}, '${key}'")

      // traverse all rows
      val iter = rows.iterator
      while (iter.hasNext && morePageExists) {

        val row = iter.next()
        val pubDate = row.findElement(By.xpath("span")).getText
        val entryLink = row.findElement(By.xpath("a"))

        driver.executeScript("arguments[0].scrollIntoView(true);", row)

        // open entry for detailed info.
        entryLink.click()
        withSnapRetries(2000) { () =>
          swithToPopupWindow(excludedWindow = mainWindowId)
        }

        // parse project details
        val entries = driver.findElementById("xwzx_content_ds")
          .findElements(By.xpath("table/tbody/tr"))

        val entryTitles = List("项目编码","项目名称","审批文号","审批文件","批复机关","批复时间",
          "受文单位","项目法人","建设地点","建设内容","建设规模")
        val projectInfo = DRProjectInfo()

        // extract entries row by row
        entries.foreach { entry =>
          val tds = entry.findElements(By.xpath("td"))

          // extract row title
          val entryTitle = tds.filter { td =>
            val t = td.getText.replaceAll("\\n", "")
            entryTitles.contains(t)
          }

          // extract row content
          val entryCont = tds.filter { td =>
            val t = td.getText.replaceAll("\\n", "")
            t != "内容摘要" && !entryTitles.contains(t)
          }

          if (entryTitle.nonEmpty) {
            val title = entryTitle.get(0).getText
            val content = if (entryCont.nonEmpty) {
              entryCont.get(0).getText
            } else {
              ""
            }

            title match {
              case "项目编码" => projectInfo.projectNumber = content
              case "项目名称" => projectInfo.projectName = content
              case "审批文号" => projectInfo.docNumber = content
              case "审批文件" => projectInfo.docName = content
              case "批复机关" => projectInfo.issuedDep = content
              case "批复时间" => projectInfo.issuedTime = content
              case "受文单位" => projectInfo.acceptDep = content
              case "项目法人" => projectInfo.legalEntity = content
              case "建设地点" => projectInfo.constLocation = content
              case "建设内容" => projectInfo.constTarget = content
              case "建设规模" => projectInfo.constScale = content
            }
          }
        }

        // record project info.
        if (projectInfo.projectNumber.length > 0) {
          logger.info(s"Project ${projectInfo.projectNumber}, ${projectInfo.issuedTime} extracted!")
          projects.put(projectInfo.projectNumber, projectInfo)
        } else {
          throw new RuntimeException("Failed to extract project details")
        }

        // determine time boundary
        try {
          val ctime = sdf.parse(projectInfo.issuedTime)
          if (ctime != null && ctime.getTime < startDateTime.getTime) {
            // Stop parsing when entries lie outside temporal range
            morePageExists = false
          }
        } catch {
          case e: ParseException =>
        }

        // (Optional) close pop-upped window
        driver.close()

        // Switch back to original window
        switchToWindowWithTimeout(driver, mainWindowId)
      }

      // Save data
      if (projects.nonEmpty) {
        dumpEntriesToDatabase(projects.values.toList)
        projects.clear()
      }

      // Switch to next page
      try {
        val pagination = driver.findElementByXPath("//div[@class='xwzx_right']/div[@class='common_page']")
        driver.executeScript("arguments[0].scrollIntoView(true);", pagination)

        val nextPageButton = pagination.findElements(By.xpath("span")).filter(_.getText == "下页")
        if (nextPageButton.nonEmpty) {
          val button = nextPageButton.get(0).findElement(By.xpath("a"))
          button.click()
          sleep(NEXTPAGE_SNAP)
        }
      } catch {
        case e: NoSuchElementException => logger.info("No more pages found")
        case e: Exception => throw e
      }

      i += 1
    }

    // Save data
    if (projects.nonEmpty) {
      dumpEntriesToDatabase(projects.values.toList)
      projects.clear()
    }
  }

  /**
    * Make web driver to focus on popuped window
    */
  private def swithToPopupWindow(excludedWindow: String) = {
    val popupWindowIds = driver.getWindowHandles.filter(_ != excludedWindow)
    if (popupWindowIds.toArray.length == 0)
      throw new RuntimeException("No popup window found")
    val popupWindow = popupWindowIds.head
    switchToWindowWithTimeout(driver, popupWindow)
  }

  /**
    * Extract pagination info, e.g. total pages count and current page index.
    */
  private def totalPage(): Tuple2[Int, Int] = {
    try {
      val currentPagePattern = new Regex("当前第(\\d+)页")
      val totalPagePattern = new Regex("共(\\d+)页")
      val pagination = driver.findElementByXPath("//div[@class='xwzx_right']/div[@class='common_page']")

      var currentPage = 1
      var totalPages = 1
      val spans = pagination.findElements(By.xpath("span"))
      spans.foreach { span =>
        val t = span.getText

        currentPagePattern.findFirstMatchIn(t).foreach { m =>
          currentPage = m.group(1).toInt
        }

        totalPagePattern.findFirstMatchIn(t).foreach { m =>
          totalPages = m.group(1).toInt
        }
      }

      (currentPage, totalPages)

    } catch {
      case e: Exception => (1, 1)
    }
  }

  /**
    * Prepare database and tables
    */
  private def validateDatabaseContext(): Unit = {
    var con: Connection = null
    var stmt: Statement = null
    try {
      con = DriverManager.getConnection(Utils.JDBC_LINK)
      stmt = con.createStatement()

      if (Utils.isMysqlBackend) {
        logger.info("Validating database as of MySQL ...")
        Class.forName("com.mysql.jdbc.Driver").newInstance()
        createTablesMysql(stmt)
      } else if (Utils.isSqlServerBackend) {
        logger.info("Validating database as of MS SQLServer ...")
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance()
        createTablesSqlServer(stmt)
      } else {
        throw new RuntimeException(s"Unsupported jdbc protocol: ${Utils.JDBC_LINK}")
      }
    } finally {
      if (con != null)
        con.close()
      if (stmt != null)
        stmt.close()
    }
  }

  private def createTablesMysql(stmt: Statement): Unit = {
    // Create backend database
    stmt.executeUpdate(s"CREATE DATABASE IF NOT EXISTS ${DATABASE_NAME} " +
      "DEFAULT CHARSET 'utf8' COLLATE 'utf8_general_ci'")

    stmt.executeUpdate(
      s"""CREATE TABLE IF NOT EXISTS ${TABLE_NAME} (
          projectNumber VARCHAR(255) PRIMARY KEY,
          projectName VARCHAR(512),
          docNumber VARCHAR(512),
          docName VARCHAR(512),
          issuedDep VARCHAR(512),
          issuedTime VARCHAR(512),
          acceptDep VARCHAR(512),
          legalEntity VARCHAR(512),
          constLocation VARCHAR(512),
          constTarget VARCHAR(512),
          constScale VARCHAR(512)
          )
          DEFAULT CHARSET utf8""".stripMargin)

    stmt.executeUpdate(s"CREATE TABLE IF NOT EXISTS ${TABLE_JSDW}  (name VARCHAR(512)) DEFAULT CHARSET utf8")
  }

  private def createTablesSqlServer(stmt: Statement): Unit = {
    def executeQuery(q: String) = {
      logger.debug(q)
      stmt.executeUpdate(q)
    }

    // Create backend database
    executeQuery(s"if db_id('${DATABASE_NAME}') is null CREATE DATABASE ${DATABASE_NAME}")

    try {
      executeQuery(s"CREATE SCHEMA ${DATABASE_NAME}")
    } catch {
      case e: SQLServerException =>
    }

    executeQuery(s"""if object_id('${TABLE_NAME}', 'U') is null
          CREATE TABLE ${TABLE_NAME} (
         |          projectNumber VARCHAR(255) PRIMARY KEY,
         |          projectName VARCHAR(512),
         |          docNumber VARCHAR(512),
         |          docName VARCHAR(512),
         |          issuedDep VARCHAR(512),
         |          issuedTime VARCHAR(512),
         |          acceptDep VARCHAR(512),
         |          legalEntity VARCHAR(512),
         |          constLocation VARCHAR(512),
         |          constTarget VARCHAR(512),
         |          constScale VARCHAR(512)
          )
      """.stripMargin
    )

    executeQuery(s"if object_id('${TABLE_JSDW}', 'U') is null CREATE TABLE ${TABLE_JSDW} (name VARCHAR(512))")
  }

  private def dumpEntriesToDatabase(projects: List[DRProjectInfo]): Unit = {
    val con = DriverManager.getConnection(JDBC_LINK)
    val stmt = con.createStatement()

    try {
      val projectsFiltered = if (FILTER_DR_CONSTRUCTORS) {
        projects.filter { p =>
          val constructor = p.legalEntity
          constructor.isEmpty ||
            validConstructors.contains(constructor) ||
            validConstructors.exists(c => constructor.contains(c))
        }
      } else {
        projects
      }

      // dump results
      projectsFiltered.foreach { s =>
        val cmd = if (isMysqlBackend) {
          s"INSERT INTO ${TABLE_NAME} VALUES ${s.format()} ON DUPLICATE KEY UPDATE projectName='${s.projectName}'"
        } else {
          s"INSERT INTO ${TABLE_NAME} VALUES ${s.format()}"
        }
        executeUpsertSilently(stmt, cmd)
      }

      logger.info(s"${projectsFiltered.length}/${projects.length} projects dumped to database")
    } finally {
      stmt.close()
      con.close()
    }
  }
}
