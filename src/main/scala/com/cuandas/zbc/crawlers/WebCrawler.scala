package com.cuandas.zbc.crawlers

import com.cuandas.zbc.Utils._
import com.typesafe.scalalogging.LazyLogging
import org.openqa.selenium.remote.RemoteWebDriver


abstract class WebCrawler extends LazyLogging {

  @volatile var driver: RemoteWebDriver = _
  // @volatile var wait: WebDriverWait = _
  @volatile var isRunning = true
  @volatile private var checkpoint = -1L
  @volatile private var runningKeys: List[String] = List.empty

  protected def heartbeat() = {
    isRunning = true
    checkpoint = System.currentTimeMillis()
  }

  protected def isHeartbeatTimeout: Boolean = {
    (checkpoint > 0) &&
      (System.currentTimeMillis() - checkpoint > HEARTBEAT_TIMEOUT)
  }

  /**
    * A robust runner of web crawler with recovery and memory
    */
  def start(keys: List[String]): Unit = {
    runningKeys = keys
    runInternal(runningKeys)
  }

  /**
    * A robust runner of web crawler with recovery and memory.
    * For each key, it performs at most RETRIES_LIMIT times of retries before
    * terminating the whole logic.
    */
  private def runInternal(keys: List[String]): Unit = {
    val totalKeys = keys.length
    var k = 0
    var retry = 0

    heartbeat()

    if (!ALWAYS_REFRESH_START) {
      driver = createWebDriver()
    }

    try {
      while (k < totalKeys && retry <= RETRIES_LIMIT) {
        try {
          if (ALWAYS_REFRESH_START) {
            driver = createWebDriver()
          }

          // parse and dump all submissions of give keyword
          searchInternal(keys(k))
          k += 1
          retry = 0
          runningKeys = keys.slice(k, keys.length)

          if (ALWAYS_REFRESH_START) {
            stopDriverQuietly(driver)
          }
        } catch {
          case e: Exception =>
            if (retry > 0)
              logger.info(s"Clear context of attempt ${retry}")

            stopDriverQuietly(driver, e)
            retry += 1
            if (retry > RETRIES_LIMIT)
              throw new RuntimeException(s"Maximum reties ${RETRIES_LIMIT} have been approached, exit")

            sleep(RETRY_SNAP)
            driver = createWebDriver()
        }
      }
    } finally {
      isRunning = false
      if (retry > RETRIES_LIMIT) {
        logger.warn(s"Closing all windows after ${RETRIES_LIMIT} retries")
      } else {
        logger.info(s"Closing all windows after finishing ${totalKeys} keys")
      }
      stopDriverQuietly(driver)
    }
  }

  def searchInternal(key: String): Unit
}
