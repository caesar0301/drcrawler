package com.cuandas.zbc

import java.net.URL
import java.sql.Statement
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.microsoft.sqlserver.jdbc.SQLServerException
import com.typesafe.scalalogging.LazyLogging
import org.jutils.jprocesses.JProcesses
import org.jutils.jprocesses.model.ProcessInfo
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.phantomjs.{PhantomJSDriver, PhantomJSDriverService}
import org.openqa.selenium.remote.{DesiredCapabilities, RemoteWebDriver}

object BrowserType extends Enumeration {
  type BrowserType = Value
  val FIREFOX, IE, EDGE, HTMLUNIT, PHANTOMJS, UNDEFINED = Value

  def withName(s: String, default: BrowserType): BrowserType = {
    try {
      BrowserType.withName(s.toUpperCase)
    } catch {
      case e: Exception => default
    }
  }
}

object Utils extends LazyLogging {

  type OptionMap = Map[Symbol, Any]

  // System settings
  val IS_DEBUGGING = System.getProperty("zbcrawler.debug", "false").toBoolean
  val ALWAYS_REFRESH_START = System.getProperty("zbcrawler.refresh.start", "false").toBoolean

  // Driver settings
  val BROWSER_DRIVER = BrowserType.withName(System.getProperty("zbcrawler.browser", "firefox"), BrowserType.UNDEFINED)
  logger.info(s"Detecting browser type: ${BROWSER_DRIVER}")
  val SELENIUM_SERVER = System.getProperty("zbcrawler.selenium.server", "localhost")
  val PHANTOMJS_EXE = System.getProperty("zbcrawler.phantomjs.executable", "driver/phantomjs")
  val DRIVER_TIMEOUT = System.getProperty("zbcrawler.driver.timeout", "180000").toInt

  // Retry snap parameters
  val RETRIES_LIMIT = System.getProperty("zbcrawler.retries", "5").toInt
  val RETRY_SNAP = System.getProperty("zbcrawler.retry.snap", "3000").toInt
  val RETRY_SNAP_STEP = System.getProperty("zbcrawler.retry.snap.step", "1000").toInt
  val RETRY_SNAP_MAX = System.getProperty("zbcrawler.retry.snap.max", "10000").toInt
  val CLICK_SNAP = System.getProperty("zbcrawler.click.snap", "5000").toInt
  val NEXTPAGE_SNAP = System.getProperty("zbcrawler.nextpage.snap", "2000").toInt

  // Smart switches
  val SWITCH_TIMEOUT = System.getProperty("zbcrawler.switch.timeout", "60000").toInt
  val REPEAT_PERIOD = System.getProperty("zbcrawler.repeat.period", "86400").toInt
  val HEARTBEAT_TIMEOUT = System.getProperty("zbcrawler.heartbeat.timeout", "30000").toInt

  val FILTER_DR_CONSTRUCTORS = System.getProperty("zbcrawler.filter.dr", "true").toBoolean

  val HISTORY_DAYS = System.getProperty("zbcrawler.history.days", "90").toInt
  val HISTORY_DAYS_DR = System.getProperty("zbcrawler.history.days.dr", HISTORY_DAYS.toString).toInt

  // private val JDBC_DEFAULT = "jdbc:mysql://localhost:3306/crawler?user=root&password=password"
  private val JDBC_DEFAULT = "jdbc:sqlserver://localhost:1433;user=admin;password=password"
  val JDBC_LINK = System.getProperty("zbcrawler.jdbc.link", JDBC_DEFAULT)

  /**
    * An automatic logic to retry parsing utility
    */
  def withSnapRetries(msec: Long)(func: () => Unit): Unit = {
    // The max snap before a utility encounters a failure
    var succeeded = false
    var snap = msec
    while (!succeeded) {
      try {
        sleep(snap)
        func()
        succeeded = true
      } catch {
        case e: Exception => {
          if (snap > RETRY_SNAP_MAX) {
            throw e
          } else {
            logger.warn((e.getMessage :: e.getStackTrace.map(_.toString) :: Nil).mkString("\n"))
            snap += RETRY_SNAP_STEP
          }
        }
      }
    }
  }

  /**
    * A logging-enabled sleeping utility
    */
  def sleep(msecs: Long): Unit = {
    Thread.sleep(msecs)
    logger.debug(s"Sleeping ${msecs} msec ...")
  }

  /**
    * Make a clean context before starting a task or after finishing a task.
    */
  def clearRunningContext(): Unit = {
    if (BROWSER_DRIVER == BrowserType.FIREFOX) {
      val processesList = JProcesses.getProcessList()
      val pids = processesList.toArray.filter { case pinfo: ProcessInfo =>
        val pname = pinfo.getName.toLowerCase
        pname.contains("firefox") || pname.contains("geckodriver") || pname.contains("werfault")
      }.map { case pinfo: ProcessInfo => pinfo.getPid }

      if (pids.nonEmpty) {
        logger.info(s"Clear running context")
        pids.foreach { pid =>
          try {
            JProcesses.killProcess(Integer.valueOf(pid)).isSuccess
          } catch {
            case e: Throwable =>
          }
        }
      }
    }
  }

  /**
    * Create a new web driver with specific settings
    */
  def createWebDriver(): RemoteWebDriver = {
    val driver = BROWSER_DRIVER match {
      case BrowserType.FIREFOX => new FirefoxDriver()
      case BrowserType.IE => {
        val capabilities = DesiredCapabilities.internetExplorer()
        capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true)
        capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true)
        capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, false)
        new InternetExplorerDriver(capabilities)
      }
      case BrowserType.EDGE => new EdgeDriver()
      case BrowserType.HTMLUNIT => {
        // FIXME: illegal character
        val capabilities = DesiredCapabilities.htmlUnitWithJs()
        new RemoteWebDriver(new URL(s"http://${SELENIUM_SERVER}:4444/wd/hub"), capabilities)
      }
      case BrowserType.PHANTOMJS => {
        // FIXME: Element does not exist in cache
        val capabilities = new DesiredCapabilities()
        capabilities.setJavascriptEnabled(true)
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, PHANTOMJS_EXE)
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX, "Y")
        // capabilities.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0")
        new PhantomJSDriver(capabilities)
      }
      case BrowserType.UNDEFINED =>
        throw new RuntimeException("Unsupported browser type")
    }
    // driver.manage().timeouts().pageLoadTimeout(DRIVER_TIMEOUT, TimeUnit.MILLISECONDS)
    driver
  }

  /**
    * Stop a web driver without disturbing main thread
    */
  def stopDriverQuietly(driver: RemoteWebDriver, t: Throwable = null): Unit = {
    if (t != null) {
      logger.error((t.getMessage :: t.getStackTrace.map(_.toString) :: Nil).mkString("\n"))
    }
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          logger.info("Stopping driver quietly, maybe!")
          driver.close()
          sleep(1000)

          // Check browser processes manually
          if (BROWSER_DRIVER == BrowserType.FIREFOX) {
            val processesList = JProcesses.getProcessList()
            val pids = processesList.toArray.filter { case pinfo: ProcessInfo =>
              pinfo.getName.toLowerCase.contains("firefox")
            }.map { case pinfo: ProcessInfo => pinfo.getPid }

            if (pids.nonEmpty) {
              logger.info(s"Firefox pids: ${pids.mkString(", ")}. Kill forcedly")
              pids.foreach { pid =>
                val success = try {
                  JProcesses.killProcess(Integer.valueOf(pid)).isSuccess
                } catch {
                  case e: Throwable => false
                }
                logger.info(s"Killing pid ${pid}: ${if (success) "succeeded" else "failed"}")
              }
              // driver.quit()
            }
          } else {
            driver.quit()
          }
        }
      }, 30, TimeUnit.SECONDS)
    } catch {
      case e: Exception => logger.error(e.toString)
    } finally {
      Thread.sleep(2000)
      clearRunningContext()
    }
  }

  def switchToWindowWithTimeout(driver: RemoteWebDriver, windowId: String): Unit = {
    try {
      TimeLimitedCodeBlock.runWithTimeout(new Runnable {
        override def run(): Unit = {
          driver.switchTo().window(windowId)
          logger.debug(s"Switched to window ${windowId}, title ${driver.getTitle}")
        }
      }, SWITCH_TIMEOUT, TimeUnit.MILLISECONDS)
    } catch {
      case e: Exception => throw e
    }
  }

  def isMysqlBackend: Boolean = {
    JDBC_LINK.split(":").apply(1).toLowerCase == "mysql"
  }

  def isSqlServerBackend: Boolean = {
    JDBC_LINK.split(":").apply(1).toLowerCase == "sqlserver"
  }

  def executeUpsert(stmt: Statement, cmd: String): Unit = {
    if (isMysqlBackend) {
      stmt.executeUpdate(cmd)
    } else {
      // SQLServer
      try {
        stmt.executeUpdate(cmd)
      } catch {
        case e: SQLServerException =>
          if (!e.toString.contains("Violation of PRIMARY KEY"))
            throw e
        case e: Exception => throw e
      }
    }
  }

  def executeUpsertSilently(stmt: Statement, cmd: String) = {
    try {
      executeUpsert(stmt, cmd)
    } catch {
      case e: Exception =>
        logger.warn((e.getMessage :: Nil).mkString("\n"))
    }
  }

  // Generate history interval (startDay, endDay) according to current time
  def historyInterval(interval: Int): (String, String) = {
    val sdf = new SimpleDateFormat("yyyy-MM-dd")
    val cl = Calendar.getInstance()
    cl.setTimeInMillis(System.currentTimeMillis())
    val end = sdf.format(cl.getTime)

    val cl2 = Calendar.getInstance()
    cl2.setTimeInMillis(cl.getTimeInMillis)
    cl2.add(Calendar.DATE, -1 * interval)
    val start = sdf.format(cl2.getTime)
    (start, end)
  }

  // Unittest
  def main(args: Array[String]): Unit = {
    val (start, end) = historyInterval(HISTORY_DAYS)
    println(start)
    println(end)
  }
}
