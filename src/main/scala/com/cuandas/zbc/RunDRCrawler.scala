package com.cuandas.zbc

import com.cuandas.zbc.Utils._
import com.cuandas.zbc.crawlers.DRCrawler
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by chenxm on 17-3-28.
  */
object RunDRCrawler extends LazyLogging {

  private var startTime = -1L

  def main(args: Array[String]): Unit = {

    while(true) {
      startTime = currentSecs()

      logger.info("Running DR crawler ...")
      DRCrawler.main(args)

      val s = REPEAT_PERIOD - secondsInterval()
      if (s > 0) {
        logger.info(s"Sleeping for ${s} seconds until next parsing ...")
        sleep(s * 1000)
      }
    }
  }

  private def secondsInterval(): Long = {
    currentSecs() - startTime
  }

  private def currentSecs(): Long = {
    (System.currentTimeMillis() / 1000).toInt
  }
}
